# ESP32 waterflow sensor
Código para un sensor de flujo de agua controlado por un ESP32, sus funcionalidades son als siguientes

- Configurable desde bluetooth
- Blink led como indicador de que no se ha conectado a ninguna red wifi
- Botón de inicio
- Contador del volumen de agua desde que se presionó el botón de inicio
- Botón de finalizar
- Dispara solicitud http a la url configurada desde bluetooth
- Reconexión del wifi en caso de que se pierda

## Configuración por bluetooth
El dispositivo permite ser configurado por bluetooth, para ello hay que bajar la aplicación [Serial Bluetooth Terminal](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal&hl=es_MX&gl=US)

una ves instalado hay que configurar la conexión bluetooth, posteriormente se podrá ejecutar comandos para establecer las configuraciones con el comando `set [config-name] [value]` las cuales son:

- ssid: nombre de la red wifi a la que se conectará
- password: contraseña de la red
- name: se mostrará para el nombre del dispositivo bluetooth y se mandará en el request
- url: url absoluta a la que se hará el request "get"

## Blink led
El ESP32 cuenta con un led integrado conectado al pin 2 de la placa, este estará "parpadeando" mientras no se logre conectar al wifi

## Botón de inicio
Comienza el conteo de litros

## Botón de finalizar
Finaliza el conteo de litros y dispara una solicitud http get a la url especificada, se le agregarán los parámetros `name` (configurado por bluetooth) y `liters` (volumen de litros)
