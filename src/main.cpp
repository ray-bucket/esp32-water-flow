#include <Arduino.h>
#include <FlowMeter.h> 
#include <Ticker.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "ConfigManager.h"
#include "Button.h"

// config handler
ConfigManager *config;

// wifi
Ticker* wifiPersistenceTicker;
Ticker* wifiBlinkTicker;
int wifiLedPin = 2; // board led
bool wifiLedPinOn = false;

// flowMeter
FlowMeter *flowMeter;
int flowMeterPin = 23;

Ticker* flowMeterTicker;
int flowMeterPeriod = 1000;

Ticker* notifyFlowTicker;
int notifyFlowPeriod = 60 * 60 * 1000;

void reloadWifi() {
  auto ssid = config->getValue("ssid");
  auto pass = config->getValue("pass");
  // Serial.print("Wifi config: ");
  // Serial.print(String("\"") + ssid);
  // Serial.println(String("\" \"") + pass + "\"");

  WiFi.begin(
    ssid.c_str(),
    pass.c_str()
  );
}

void sendRequest(double liters) {
  auto url = config->getValue("url");
  auto name = config->getValue("name");

  if (url.length() == 0 || name.length() == 0) {
    return;
  }

  auto requestUrl = url + String("?name=") + name + String("&liters=") + String(liters);

  HTTPClient http;
  http.begin(requestUrl.c_str());
  int status = http.GET();
  if (status > 0) {
    Serial.print("HTTP Response code: ");
    Serial.println(status);
    // String payload = http.getString();
    // Serial.println(payload);
  }
  http.end();
}

void setup() {
  Serial.begin(115200);
  // Serial.println("Setup");

  // flowMeter counter by interruption
  flowMeter = new FlowMeter(digitalPinToInterrupt(flowMeterPin), UncalibratedSensor, [](){
    flowMeter->count();
  } , RISING);

  // flowMeter timer (1 sec. precision)
  flowMeterTicker = new Ticker([](){
    flowMeter->tick(flowMeterPeriod);
  }, flowMeterPeriod, 0, MILLIS);
  flowMeterTicker->start();

  // wifi persistence
  wifiPersistenceTicker = new Ticker([](){
    if (WiFi.status() != WL_CONNECTED) {
      reloadWifi();
    }
  }, 10* 1000, 0, MILLIS);
  wifiPersistenceTicker->start();

  // wifi led
  pinMode(wifiLedPin, OUTPUT);
  wifiBlinkTicker = new Ticker([](){
    if (WiFi.status() != WL_CONNECTED) {
      wifiLedPinOn = !wifiLedPinOn;
    } else {
      wifiLedPinOn = false;
    }
    digitalWrite(wifiLedPin, wifiLedPinOn);
  }, 200, 0, MILLIS);
  wifiBlinkTicker->start();

  // send flowmeter count
  notifyFlowTicker = new Ticker([](){
    double liters = flowMeter->getTotalVolume();
    flowMeter->reset();
    flowMeter->setTotalVolume(0);

    sendRequest(liters);
  }, notifyFlowPeriod, 0, MILLIS);
  notifyFlowTicker->start();

  // updates when config has been updated
  config = new ConfigManager("config");
  config->define("ssid", [](String ssid){
    // Serial.println(String("Ssid changed, reset wifi ") + ssid);
    reloadWifi();
  });
  config->define("pass", {"password"}, [](String pass){
    // Serial.println(String("Password changed, reset wifi ") + pass);
    reloadWifi();
  });
  config->define("name", [](String name) {
    // Serial.println(String("Name changed, rename bluetooth ") + name);
    config->beginBluetooth(String("Waterflow ") + name);
  });
  config->define("url", {"server"}, [](String url){
    // Serial.println(String("Url changed ") + url);
  });
  String name = String("Waterflow ") + config->getValue("name");
  config->beginBluetooth(name);

  // wifi
  reloadWifi();

  // Serial.println("End setup");
}

void loop() {
  config->tick();
  notifyFlowTicker->update();
  flowMeterTicker->update();
  wifiBlinkTicker->update();
  wifiPersistenceTicker->update();
}
