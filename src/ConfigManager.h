#include <Arduino.h>
#include <BluetoothSerial.h>
#include <SimpleCLI.h>
#include <ArduinoNvs.h>
#include <LinkedList.h>

using OnChange = std::function<void(String)>;

struct ConfigNode {
  String name;
  std::vector<String> aliases;
  OnChange onChange;
};

/*
  This class do the folowing thinks
    - Handle bluethoot connection
    - Bluetooth cli to set config
    - Store configs
    - Get stored config from outside class by getters
    - Support for subscriptions when config has been changed by bluetooth
*/
class ConfigManager {
private:
  String name;
  SimpleCLI cli;
  ArduinoNvs store;
  BluetoothSerial connection;
  std::vector<ConfigNode> configs;
  const char* nameSpace;

  ConfigNode getConfigNode(String name) {
    for (auto config : this->configs) {
      if (config.name == name) {
        return config;
      }

      for (auto alias : config.aliases) {
        if (alias == name) {
          return config;
        }
      }
    }

    return ConfigNode();
  }

  void handleSet(Command& command) {
    auto nameArg = command.getArgument(0);
    auto valueArg = command.getArgument(1);
    if (!nameArg.isSet() || !valueArg.isSet()) return;

    auto configNode = this->getConfigNode(nameArg.getValue());
    if (configNode.name == "") {
      this->connection.println("Config not found");
      return;
    }

    // store data
    // auto name = nameArg.getValue();
    auto value = valueArg.getValue();
    this->store.setString(configNode.name, value);

    // notify
    if (configNode.onChange) {
      configNode.onChange(value);
    }

    this->connection.println("Ok");
  }

public:
  ConfigManager(const char* nameSpace) {
    this->nameSpace = nameSpace;
    this->store.begin(this->nameSpace);

    this->cli.addBoundlessCommand("set");
  }

  ConfigManager* beginBluetooth(String name) {
    this->name = name;

    this->connection.end();
    this->connection.begin(this->name);
    this->connection.setPin("waterflow");
    return this;
  }

  ConfigManager* tick() {
    if (this->connection.available()) {
      auto command = this->connection.readString();
      this->cli.parse(command);
    };

    if (!this->cli.available()) return this;

    auto command = this->cli.getCommand();

    if (command.getName() == "set") {
      this->handleSet(command);
    } else {
      this->connection.println("Bad command");
    }

    return this;
  }

  ConfigManager* define(String name, std::vector<String> aliases, OnChange onChange = NULL) {
    ConfigNode config;
    config.name = name;
    config.aliases = aliases;
    config.onChange = onChange;

    this->configs.push_back(config);

    return this;
  }

  ConfigManager* define(String name, OnChange onChange = NULL) {
    return this->define(name, std::vector<String>(), onChange);
  }

  String getValue(const char* name) {
    return this->store.getString(name);
  }
};
