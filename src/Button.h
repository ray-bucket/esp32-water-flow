#include <Arduino.h>

using Callback = std::function<void()>;

class Button {
private:
  int pin;
  Callback callback;
public:
  Button(int pin, Callback callback) {
    pinMode(pin, INPUT_PULLUP);
    this->pin = pin;
    this->callback = callback;
  }

  void tick() {
    auto isActive = digitalRead(this->pin) == 0; // for ESP32, 0 is "active"
    if (isActive) {
      this->callback();
    }
  }
};
